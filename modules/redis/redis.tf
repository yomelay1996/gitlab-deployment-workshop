resource "aws_security_group" "redis" {
  name        = "redis-sg"
  description = "redis security group"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 6379
    to_port     = 6379
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
    #cidr_blocks = [var.vpc_cidr]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_elasticache_subnet_group" "subnets" {
  name       = "redis-subnet-group"
  subnet_ids = var.private_subnets
}

resource "aws_elasticache_replication_group" "redis" {
  replication_group_id          = "gl-rep-group-2"
  replication_group_description = "replication group"
  number_cache_clusters         = var.number_cache_clusters
  engine_version   = "5.0.5"
  engine                        = "redis"
  node_type                     = var.node_type
  automatic_failover_enabled    = true
  subnet_group_name             = aws_elasticache_subnet_group.subnets.name
  security_group_ids            = ["${aws_security_group.redis.id}"]
  port                          = 6379
}
