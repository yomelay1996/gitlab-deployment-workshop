#!/usr/bin/env bash

mkdir -p /etc/gitlab-runner
cat > /etc/gitlab-runner/config.toml <<- EOF

${runner_config}

EOF

yum install jq -y

curl --request POST "${GitLabURL}/api/v4/runners" --form "token=${GitLabRegToken}" --form "description=${RunnerDescription}" -s | jq .token --raw-output > /etc/gitlab-runner/regtoken
regtoken=$(cat /etc/gitlab-runner/regtoken) && sed -i "s/GitLabRegToken/$regtoken/g" /etc/gitlab-runner/config.toml
curl -L https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.rpm.sh | sudo bash
export GITLAB_RUNNER_DISABLE_SKEL=true
yum install gitlab-runner -y

amazon-linux-extras install docker
usermod -a -G docker ec2-user
systemctl enable docker
systemctl start docker

curl https://gitlab-docker-machine-downloads.s3.amazonaws.com/master/docker-machine-Linux-x86_64 --output /usr/local/bin/docker-machine

chmod +x /usr/local/bin/docker-machine
ln -s /usr/local/bin/docker-machine /usr/bin/docker-machine


service gitlab-runner restart$
