output "gitaly_private_ip" {
  value = aws_instance.gitaly_server.*.private_ip
}

output "praefect_endpoint" {
  value = aws_lb.nlb.dns_name  
}

output "praefect_private_ip" {
  value = aws_instance.praefect_server.*.private_ip
}

output "rds_endpoint" {
  value = aws_db_instance.gitlab-pg.address
}
output "rds_db_name" {
  value = "praefect_production"
}
output "rds_admin_user" {
  value = aws_db_instance.gitlab-pg.username
}
output "rds_admin_pass" {
  value = aws_secretsmanager_secret_version.gitlab-postgres-password.secret_string
}
