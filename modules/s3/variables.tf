variable "prefix" {}

variable "gitlab_s3_buckets" {
  type        = list
  description = "list of s3 buckets to creates"
}

variable "s3_kms_key_arn" {
  type    = string
  default = null
}

variable "tags" {
  type        = map(string)
  description = "map of tags to put on the resource"
  default     = {}
}
