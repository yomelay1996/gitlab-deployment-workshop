variable ami_id {
    description = "AMI ID for Instance Launch"
}

variable key_name {
    description = "AWS Key Pair for SSH"
}

variable "instance_type" {
    description = "Instance Type"
    default = "t3.large"
}

variable "rails_count" {
    description = "Number of Rails Servers"
    default = 1
  
}

variable "private_subnets" {
    description = "Private Submnets for Rails Deployment"
}

variable "public_subnets" {
    description = "Private Submnets for LoadBalancer Deployment"
}

variable "vpc_id" {
    description = "VPC For the Instance"
}

variable "vpc_cidr" {
    description = "VPC Cidr for Security Groups"
}

variable "prefix" {
    description = "S3 Bucket Prefix to Simplify IAM"
}