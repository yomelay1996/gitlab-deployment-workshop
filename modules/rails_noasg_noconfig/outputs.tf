output "rails_ips" {
    value = aws_instance.rails.*.private_ip
}

output "initial_rails_ip" {
    value = aws_instance.rails[0].private_ip
}

output "lb_url" {
    value = aws_elb.rails.dns_name
}