
resource "aws_elb_attachment" "rails" {
    count = var.rails_count
  elb      = aws_elb.rails.id
  instance = aws_instance.rails[count.index].id
}

resource "aws_elb" "rails" {
  name               = "Gitlab-Rails-LB"
  subnets            = var.public_subnets
#   availability_zones = var.availability_zones

  listener {
    instance_port     = 80
    instance_protocol = "http"
    lb_port           = 80
    lb_protocol       = "http"
  }

  listener {
    instance_port      = 443
    instance_protocol  = "tcp"
    lb_port            = 443
    lb_protocol        = "tcp"
  }

  listener {
    instance_port     = 22
    instance_protocol = "tcp"
    lb_port           = 22
    lb_protocol       = "tcp"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 3
    target              = "HTTP:80/-/health"
    interval            = 30
  }


  security_groups           = [aws_security_group.rails_lb.id]
  cross_zone_load_balancing = true
}

resource "aws_security_group" "rails_lb" {
  name        = "gitlab-lb-sg"
  description = "Rails LB Security Group"
  vpc_id      = var.vpc_id

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}