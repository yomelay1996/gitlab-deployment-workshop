Role Name
=========

This Role is to deploy Linux Host keys to Rails Servers

Requirements
------------

Copy the host keys from your first rails box to the files directory (remove placeholder)
There was an initial iteration that groups the keys from the first box and populates 
the keys for the rest of the node, but a Customer replaced the irst node and caused
Git issues. Left it in commented out as an example.

Role Variables
--------------


Optional (Read Above)
initial_rails:                     Initial Rails Host names (Or Index 0)


Dependencies
------------
The following keys placed in files
    - "ssh_host_rsa_key"
    - "ssh_host_ecdsa_key"
    - "ssh_host_ed25519_key"
    - "ssh_host_rsa_key.pub"
    - "ssh_host_ecdsa_key.pub"
    - "ssh_host_ed25519_key.pub"

Author Information
------------------

kvogt@gitlab.com