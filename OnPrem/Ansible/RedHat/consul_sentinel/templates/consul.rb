roles ['consul_role', 'redis_sentinel_role']
consul['configuration'] = {
  server: true,
  bind_addr: '{{ ansible_eth0.ipv4.address }}',
  retry_join: %w({{ consul_servers }})
}

gitlab_rails['auto_migrate'] = false
consul['monitoring_service_discovery'] =  true
node_exporter['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9100'

redis['master_name'] = 'gitlab-redis'
redis['master_password'] = '{{ redis_password }}'
redis['master_ip'] = '{{ redis_master_ip }}'

redis['port'] = 6379
sentinel['bind'] = '{{ ansible_eth0.ipv4.address }}'
sentinel['quorum'] = 2
# sentinel['port'] = 26379