Role Name
=========

GitLab Rails and SideKiq Roles

ToDo: 
* Create an Independent Sidekiq role and parametize sidekiq configurations
* Switch SideKiq to Sidekiq Cluster
* Clean up SMTP / LDAP Etc Blocks, possible replace with a Straight Multistring passthrough with these types of customizations.
* Solution for omniauth, commented out at the moment
* Geo Assumes Cloud Based Postgres Solution. Support for Omnibus in Geo needs to be added.

Requirements
------------

Place gitlab-secrets.json in the templates directory. gitlabhq_production database name is assumed.
Verify SMTP and Other Configurations are 

Clean up gitlab.rb template to ensure we are not delivering functionality the Customer will not use.

gitlabhq_geo_production is assumed for the Geo Tracking Database

Role Variables
--------------

external_url                        : URL Of GitLab
redirect_http_to_https              : NGINX Boolean to determine if redirection to https is required. Should be set to yes.
monitoring_whitelist                : CIDR for Monitoring White List. Rails Load Balancer should be considered for health checks.

initial_rails                       : Initial (First Rails Server)
init_db                             : Boolean to indicate if the database should be seeded

postgres_user                       : Postgres User Name

pg_password                         : Postgres Password
pgbouncer_host                      : PGBouncer Host / Or Postgres Hostname if not using pooling
db_port                             : Database Host 5432 for PG 6432 for PGBouncer

redis_master                        : Redis Master Address
redis_port                          : Default port is 6379
redis_ssl                           : boolean

gitaly_token                        : Gitaly Token

object_store_provider               : AWS or AzureRM

object_storage_region               : AWS Region (Any value can be used for Azure)
artifacts_bucket                    : Artifact Bucket Name
externaldiffs_bucket                : External Diffs Bucket Name
lfs_bucket                          : LFS Bucket Name
uploads_bucket                      : Uploads Bucket Name
packages_bucket                     : Packages Bucket Name
dependencyproxy_bucket              : Dependency Proxy Bucket Name
terraformstate_bucket               : Terraform Bucket Name
pages_bucket                        : Pages Bucket Name
backup_bucket                       : Backup Bucket Name

Optional

pages_enabled                       : Enable GitLab Pages
rails_cert_name                     : Certificate for Rails if Using SSL

geo_enabled                         : boolean reserved for the primary geo instance
geo_secondary                       : boolean to indicate if this is a geo secondary node
geo_node_name                       : Geo Node Name 
tracking_db_user                    : UserName for Tracking Database (Geo Log Cursor)
tracking_db_password                : Password for Tracking Database (Geo Log Cursor)
geo_tracking_db_host                : Host for Tracking Database (Geo Log Cursor)

pgbouncer_users_pg_bouncer_pw       : PGBouncer Password (Hash?)

object_storage_access_key_id        : AWS Or Azure Key (User Name)
object_storage_secret_access_key    : AWS or Azure Secret Key

redis_password                      : Reis Password
redis_sentinel_list                 : List of Sentinels

praefect_ext_token                  : External Praefect Token
praefect_protocol                   : Praefect protocol (tls or tcp)
praefect_host                       : Praefect Host or Load Balancer Name
praefect_port                       : Praefect Port

smtp_enabled                        : Boolean for SMTP
gitlab_email_reply_to               : SMTP Reply To
smtp_user                           : SMTP Authentication
smtp_pass                           : SMTP Authentication
smtp_address                        : SMTP Address
smtp_reply                          : SMTP Email From (Not Reply to)

ldap_enabled                        : Boolean indicating if LDAP is enabled
ldap_host                           : LDAP Host
ldap_port                           : LDAP Port
ldap_bind_dn                        : LDAP Bind DN (DN Of Bind Account for Auth)
ldap_password                       : Bind DN Password
ldap_group_base                     : Base to begin group search.

consul_servers                      : Example consul_servers: "'51.89.225.239 51.89.225.38 51.89.227.109'"
monitoring_ip                       : IP Of the Grafana / Prometheus Node

gravatar_enabled                    : boolean to enable / disable gravata

Dependencies
------------

GitLab Already Installed, or gl_install role.

Example
------------
ansible-playbook -i inventory gl_rails.yml

Author Information
------------------

kvogt@gitlab.com


