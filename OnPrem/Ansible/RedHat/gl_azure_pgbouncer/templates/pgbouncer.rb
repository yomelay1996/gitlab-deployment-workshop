pgbouncer['enable'] = true
pgbouncer['databases'] = {
  {{SWCI}}gitlabhq_production: {
    host: '{{postgres_master}}',
    user: '{{postgres_user}}',
    port: 5432,
###! generate this with `echo -n '$password + $username' | md5sum`
    password: '{{pg_password}}'
  }
}
node_exporter['listen_address'] = '0.0.0.0:9100'
# pgbouncer['client_tls_ca_file'] = nil
pgbouncer['client_tls_key_file'] = '/etc/gitlab/ssl/{{pgbouncer_cert_name}}.key'
pgbouncer['client_tls_cert_file'] = '/etc/gitlab/ssl/{{pgbouncer_cert_name}}.crt'

# pgbouncer_exporter['enable'] = false
# pgbouncer_exporter['log_directory'] = "/var/log/gitlab/pgbouncer-exporter"
# pgbouncer_exporter['listen_address'] = 'localhost:9188'
# pgbouncer_exporter['env_directory'] = '/opt/gitlab/etc/pgbouncer-exporter/env'
# pgbouncer_exporter['env'] = {
#   'SSL_CERT_DIR' => "/opt/gitlab/embedded/ssl/certs/"
# }
pgbouncer['enable'] = true
praefect['enable'] = false
postgresql['enable'] = false
redis['enable'] = false
nginx['enable'] = false
prometheus['enable'] = false
grafana['enable'] = false
puma['enable'] = false
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
gitaly['enable'] = false

pgbouncer['admin_users'] = %w(pgbouncer {{postgres_user}})
pgbouncer['server_tls_sslmode'] = 'verify-ca' #'allow'


#pgbouncer['auth_type'] = 'trust'

#This Certificate is public microsoft certificate for Azure Postgres. It expires 2025
pgbouncer['server_tls_ca_file'] = '/etc/root.crt' # '/etc/gitlab/trusted-certs/ca.crt'

pgbouncer['auth_type'] = 'plain'

pgbouncer['min_pool_size'] = '100'

