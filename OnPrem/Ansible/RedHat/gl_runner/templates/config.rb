concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "{{runner_name}}"
  url = "{{gitlab_url}}"
  token = "{{runner_token}}"
  executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "alpine:latest"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
  [runners.machine]
    IdleCount = 0
    MachineDriver = ""
    MachineName = ""
    OffPeakTimezone = ""
    OffPeakIdleCount = 0
    OffPeakIdleTime = 0