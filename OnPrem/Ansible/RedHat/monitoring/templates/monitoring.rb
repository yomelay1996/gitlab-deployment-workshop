external_url '{{external_url}}'

# Enable Prometheus
prometheus['enable'] = true
prometheus['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9090'
prometheus['monitor_kubernetes'] = false

# Enable Grafana
grafana['enable'] = true
grafana['disable_login_form'] = false
grafana['admin_password'] = '{{grafana_admin_pwd}}'

#######################################
###     Monitoring configuration    ###
#######################################

# Enable service discovery for Prometheus
consul['enable'] = true
consul['monitoring_service_discovery'] =  true
gitlab_rails['monitoring_whitelist'] = ['{{monitoring_whitelist}}']

{% if smtp_host is defined %}
alertmanager['enable'] = true
alertmanager['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9093'
alertmanager['global'] = {
  'smtp_from' => '{{smtp_from}}',   # Alertmanager copy of SMTP configuration
  'smtp_smarthost' => '{{smtp_host}}:{{smtp_port}}',
}
{% endif %}

{% if consul_servers is defined %}
# Replace placeholders
# Y.Y.Y.Y consul1.gitlab.example.com Z.Z.Z.Z
# with the addresses of the Consul server nodes
consul['configuration'] = {
   bind_addr: '{{ ansible_eth0.ipv4.address }}',
   retry_join: %w({{consul_servers}}),
}
{% endif %}

# Set the network addresses that the exporters will listen on
node_exporter['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9100'

# NGINX Status for prometheus
nginx['status']['options']['allow'] = ['{{ ansible_eth0.ipv4.address }}']
nginx['status']['listen_addresses'] = ['{{ ansible_eth0.ipv4.address }}']

# Intentionally commented out because of the customizations required.
# prometheus['scrape_configs'] = [
#   {
#     'job_name': 'NGINX',
#     'static_configs' => [
#       'targets' => %w(ARRAY OF TARGETS)
#     ],
#   },
#   {
#     'job_name': 'gitlab-workhorse',
#     'static_configs' => [
#       'targets' => %w(ARRAY OF TARGETS)
#     ],
#   },
#   {
#     'job_name': 'gitlab-rails',
#     'metrics_path': '/-/metrics',
#     'static_configs' => [
#       'targets' => %w(ARRAY OF TARGETS)
#     ],
#   },
#   {
#     'job_name': 'node',
#     'static_configs' => [
#       'targets' => %w(ARRAY OF TARGETS)
#     ],
#   },
#   {
#     'job_name': 'gitlab-sidekiq-exporter',
#     'static_configs' => [
#       'targets' => %w(ARRAY OF TARGETS)
#     ],
#   },
#   {
#     'job_name': 'gitaly',
#     'static_configs' => [
#       'targets' => %w(ARRAY OF TARGETS)
#     ],
#   },
#   {
#     'job_name': 'praefect',
#     'static_configs' => [
#       'targets' => %w(ARRAY OF TARGETS)
#     ],
#   },
# ]



## END Monitoting

# Disable all other services
gitlab_rails['auto_migrate'] = false
gitaly['enable'] = false
gitlab_exporter['enable'] = false
gitlab_workhorse['enable'] = false
nginx['enable'] = true
postgres_exporter['enable'] = false
postgresql['enable'] = false
redis['enable'] = false
redis_exporter['enable'] = false
sidekiq['enable'] = false
unicorn['enable'] = false
puma['enable'] = false
#node_exporter['enable'] = false
