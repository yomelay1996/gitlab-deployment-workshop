# /etc/gitlab/gitlab.rb

## Avoid running unnecessary services on the Gitaly server
postgresql['enable'] = false
redis['enable'] = false
nginx['enable'] = false
prometheus['enable'] = false
puma['enable'] = false
unicorn['enable'] = false
sidekiq['enable'] = false
gitlab_workhorse['enable'] = false
grafana['enable'] = false
alertmanager['enable'] = false
gitaly['enable'] = true

# gitaly['env'] = {
#   'GODEBUG' => 'x509ignoreCN=0',
#   'GITALY_DISABLE_REF_TRANSACTIONS' => 'true'
# }

## Prevent database connections during 'gitlab-ctl reconfigure'
gitlab_rails['rake_cache_clear'] = false
gitlab_rails['auto_migrate'] = false

## Configure the gitlab-shell API callback URL. Without this, `git push` will
## fail. This can be your 'front door' GitLab URL or an internal load
## balancer.
## Don't forget to copy `/etc/gitlab/gitlab-secrets.json` from web server to Gitaly server.
gitlab_rails['internal_api_url'] = "{{external_url}}"

## Make Gitaly accept connections on all network interfaces. You must use
## firewalls to restrict access to this address/port.
gitaly['listen_addr'] = '0.0.0.0:8075'
node_exporter['listen_address'] = '0.0.0.0:9100'

{% if gitaly_tls is defined and gitaly_tls %}
gitaly['tls_listen_addr'] = "0.0.0.0:9999"
gitaly['certificate_path'] = "/etc/gitlab/ssl/{{gitaly_cert_name}}.crt"
gitaly['key_path'] = "/etc/gitlab/ssl/{{gitaly_cert_name}}.key"
{% endif %}

{% if praefect_int_token is not defined %}
gitaly['auth_token'] = '{{gitaly_token}}'

gitaly['storage'] = [
  {'name' => "default", 'path' => "/gitlab-data" },
]

{% endif %}

{% if praefect_int_token is defined %}
gitaly['auth_token'] = '{{ praefect_int_token }}'
gitlab_shell['secret_token'] = '{{gitaly_token}}'

git_data_dirs({
  "gitaly-1" => {
    "path" => "/gitlab-data"
  },
  "gitaly-2" => {
    "path" => "/gitlab-data"
  },
  "gitaly-3" => {
    "path" => "/gitlab-data"
  }
})

{% endif %}


{% if consul_servers is defined %}
# Enable service discovery for Prometheus
consul['enable'] = true
consul['monitoring_service_discovery'] = true

consul['configuration'] = {
  bind_addr: '{{ ansible_eth0.ipv4.address }}',
  retry_join: %w({{ consul_servers }})
}
{% endif %}

# Set the network addresses that the exporters will listen on
node_exporter['listen_address'] = '{{ ansible_eth0.ipv4.address }}:9100'
gitaly['prometheus_listen_addr'] = '{{ ansible_eth0.ipv4.address }}:9236'
